<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        // models
        \App\Repositories\Contracts\BarRepositoryInterface::class => \App\Repositories\BarRepository::class,
        \App\Repositories\Contracts\HookahRepositoryInterface::class => \App\Repositories\HookahRepository::class,
        \App\Repositories\Contracts\ReservationRepositoryInterface::class => \App\Repositories\ReservationRepository::class,
        // services
        \App\Services\Contracts\ComboGenerator::class => \App\Services\HookahComboGenerator::class,
        \App\Services\Contracts\ComboFilter::class => \App\Services\HookahComboFilter::class,
        \App\Services\Contracts\HookahFindAvailableInterface::class => \App\Services\HookahFindAvailable::class,
        // rules
        \App\Rules\Contracts\DateHookahAvailabilityInterface::class => \App\Rules\DateHookahAvailability::class,
        \App\Rules\Contracts\UserCountPipesSatisfyInterface::class => \App\Rules\UserCountPipesSatisfy::class,
    ];

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
