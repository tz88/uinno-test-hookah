<?php

namespace App\Services;

use App\Repositories\Contracts\HookahRepositoryInterface;
use App\Repositories\Contracts\ReservationRepositoryInterface;

class HookahFindAvailable implements \App\Services\Contracts\HookahFindAvailableInterface
{
    /** @var HookahRepositoryInterface */
    private $hookahs;

    /** @var ReservationRepositoryInterface */
    private $reservations;


    /**
     * HookahComboGenerator constructor.
     * @param HookahRepositoryInterface $hookahs
     * @param ReservationRepositoryInterface $reservations
     */
    public function __construct(HookahRepositoryInterface $hookahs, ReservationRepositoryInterface $reservations)
    {
        $this->hookahs = $hookahs;
        $this->reservations = $reservations;
    }

    /**
     * @inheritDoc
     */
    public function find(int $barId, array $data = [])
    {
        // select hookah ids that are reserved for provided time
        $reservationQuery = $this->reservations->select();
        $reservationQuery = $this->reservations->byReservationDate($reservationQuery, $data['from'], $data['to']);
        $hookahsBusyIds = $reservationQuery->distinct()->pluck('hookah_id');
        // select all hookahs that are available for provided date (no matter of pipes_count)
        $hookahQuery = $this->hookahs->select()->whereNotIn('id', $hookahsBusyIds);
        $hookahQuery = $this->hookahs->byBar($hookahQuery, $barId);
        return $hookahQuery->get();
    }
}
