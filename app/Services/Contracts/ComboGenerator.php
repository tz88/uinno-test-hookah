<?php

namespace App\Services\Contracts;

interface ComboGenerator
{
    /**
     * Generate combination of models satisfying some specific params
     *
     * @param array $items
     * @param int $userCount
     * @return array
     */
    public function generate(iterable $items, int $userCount): array;
}
