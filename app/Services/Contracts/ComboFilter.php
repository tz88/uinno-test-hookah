<?php

namespace App\Services\Contracts;

interface ComboFilter
{
    /**
     * Filters provided data in order to remove combos that doesn't match and simplify results array
     *
     * @param array $data rough data
     * @param string $fieldName field that should be checked
     * @param int $countValue count that should be satisfied
     * @return array
     */
    public function apply(array $data, string $fieldName, int $countValue): array;
}
