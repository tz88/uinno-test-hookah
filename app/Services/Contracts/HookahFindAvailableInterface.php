<?php

namespace App\Services\Contracts;

interface HookahFindAvailableInterface
{
    /**
     * Find list of available hookahs by provided params
     *
     * @param int $barId
     * @param array $data
     * @return mixed
     */
    public function find(int $barId, array $data = []);
}
