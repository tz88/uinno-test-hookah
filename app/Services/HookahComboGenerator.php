<?php

namespace App\Services;

use App\Contracts\UserCountableInterface;
use App\Services\Contracts\ComboFilter;

class HookahComboGenerator implements \App\Services\Contracts\ComboGenerator
{
    /** @var array */
    private $items;

    /** @var integer */
    private $userCount;

    /** @var integer */
    private $filter;

    /** @var \App\Services\Contracts\ComboFilter */
    private $combos = [];

    /**
     * HookahComboGenerator constructor.
     *
     * @param App\Services\Contracts\ComboFilter $filter
     */
    public function __construct(ComboFilter $filter)
    {
        $this->filter = $filter;
    }

    /**
     * @inheritDoc
     */
    public function generate(iterable $items, int $userCount): array
    {
        $this->items = $items;
        $this->userCount = $userCount;
        // get list of items that satisfy userCount alone - they should come in the priority and dont mess with combinations
        $this->combineSingles();
        // add to list combos that left
        $key = count($this->combos);
        $this->combineOther($key);
        // filter results before returning
        return $this->filter->apply($this->combos, 'userCount', $this->userCount);
    }

    /**
     * Add item into $combos storage
     * @param UserCountableInterface $item
     * @param int $key
     * @return bool
     */
    private function add(UserCountableInterface $item, int $key): bool
    {
        // new combination
        if (!isset($this->combos[$key])) {
            $this->combos[$key] = ['items' => [], 'userCount' => 0];
        }
        // item already exists in combination
        if (isset($this->combos[$key]['items'][$item->id])) {
            return false;
        }
        // add item
        $this->combos[$key]['items'][$item->id] = $item;
        $this->combos[$key]['userCount'] += $item->user_count;
        return true;
    }

    /**
     * Get single items which satisfy userCount
     */
    private function combineSingles()
    {
        $key = 0;
        $restItems = [];
        foreach ($this->items as $item) {
            if ($item->userCount >= $this->userCount) {
                $this->add($item, $key);
                $key++;
            } else {
                $restItems[] = $item;
            }
        }
        $this->items = $restItems;
    }

    /**
     * Get different variants of combinations of items to satisfy userCount
     * This method is not fully correct but won't waste time polishing it, the main idea should be clear
     * @param int $key
     * @param int $i
     */
    private function combineOther(&$key = 0, $i = 0)
    {
        if ($i >= count($this->items)) {
            return;
        }
        for ($i; $i < count($this->items); $i++) {
            $item = $this->items[$i];
            $this->add($item, $key);
            // if summary userCount is ok -> proceed to next item
            if ($this->combos[$key]['userCount'] >= $this->userCount) {
                $key++;
                continue;
            } else {
                // is total user count is still less that required - we continue collecting items to the same combo
                $this->combineOther($key, $i + 1);
            }
        }
    }
}
