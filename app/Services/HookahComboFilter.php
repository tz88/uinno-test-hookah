<?php

namespace App\Services;

use App\Contracts\UserCountableInterface;

class HookahComboFilter implements \App\Services\Contracts\ComboFilter
{
    /**
     * @inheritDoc
     */
    public function apply(array $data, string $fieldName, int $countValue): array
    {
        $cleanCombos = [];
        foreach ($data as $key => $combo) {
            // user count that does not satisfy us (probably last combo)
            if (isset($combo[$fieldName])) {
                // check if combo satisfy our criteria
                if ($combo[$fieldName] < $countValue) {
                    continue;
                }
                // remove unneeded temporary fields
                unset($data[$key][$fieldName]);
            }
            // getting common keys to remove duplications
            $this->addItem($cleanCombos, $combo);
        }
        return array_values($cleanCombos);
    }

    /**
     * Add item to clean storage
     * @param array $storage
     * @param array $itemsCombo
     */
    private function addItem(array &$storage, array $itemsCombo)
    {
        $key = '';
        ksort($itemsCombo['items']);
        foreach ($itemsCombo['items'] as $item) {
            $key .= $item->id;
        }
        $storage[$key] = array_values($itemsCombo['items']);
    }
}
