<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bar extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hookahs()
    {
        return $this->hasMany('App\Hookah');
    }

    /**
     * Get the Bar through Hookah.
     */
    public function reservations()
    {
        return $this->hasManyThrough('App\Reservation', 'App\Hookah');
    }
}
