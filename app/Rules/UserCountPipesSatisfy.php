<?php

namespace App\Rules;

use App\Repositories\Contracts\HookahRepositoryInterface;
use App\Rules\Contracts\UserCountPipesSatisfyInterface;
use Illuminate\Contracts\Validation\Rule;

class UserCountPipesSatisfy implements Rule, UserCountPipesSatisfyInterface
{
    /** @var int */
    private $hookahId;

    /**
     * The reservations repository instance.
     *
     * @var \App\Repositories\Contracts\HookahRepositoryInterface
     */
    private $hookahs;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $hookahId, HookahRepositoryInterface $hookahs)
    {
        $this->hookahId = $hookahId;
        $this->hookahs = $hookahs;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $hookah = $this->hookahs->get($this->hookahId);
        return $value <= ($hookah->pipes_count * $hookah::USERS_PER_PIPE);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Current hookah does not have enough pipes for selected number of people.';
    }
}
