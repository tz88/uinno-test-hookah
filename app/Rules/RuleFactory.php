<?php

namespace App\Rules;

/**
 * Class RuleFactory is responsible for instantiating custom Rules
 * @package App\Rules
 */
class RuleFactory
{
    /**
     * Rules to instantiate that are available
     */
    public const RULE_DATE_HOOKAH_AVAILABILITY = 'date_hookah_availability';
    public const RULE_USER_COUNT_PIPES_SATISFY = 'user_count_pipes_satisfy';

    /**
     * @var string[] Rule names to Contracts mapping
     */
    private static $definitions = [
        self::RULE_DATE_HOOKAH_AVAILABILITY => \App\Rules\Contracts\DateHookahAvailabilityInterface::class,
        self::RULE_USER_COUNT_PIPES_SATISFY => \App\Rules\Contracts\UserCountPipesSatisfyInterface::class,
    ];

    public static function create(string $ruleType, $params = []): \Illuminate\Contracts\Validation\Rule
    {
        // validate type
        if (!isset(static::$definitions[$ruleType])) {
            throw new \InvalidArgumentException("Rule type {$ruleType} is not supported.");
        }

        // instantiating
        return app()->make(
            static::$definitions[$ruleType],
            $params
        );
    }
}
