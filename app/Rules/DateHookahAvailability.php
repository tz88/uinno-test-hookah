<?php

namespace App\Rules;

use App\Repositories\Contracts\ReservationRepositoryInterface;
use App\Rules\Contracts\DateHookahAvailabilityInterface;
use Illuminate\Contracts\Validation\Rule;

class DateHookahAvailability implements Rule, DateHookahAvailabilityInterface
{
    /** @var int */
    private $hookahId;

    /**
     * The reservations repository instance.
     *
     * @var \App\Repositories\Contracts\ReservationRepositoryInterface
     */
    private $reservations;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(int $hookahId, ReservationRepositoryInterface $reservations)
    {
        $this->hookahId = $hookahId;
        $this->reservations = $reservations;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->reservations->getByDateDiapason($this->hookahId, $value)->doesntExist();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'This hookah is not available for selected date.';
    }
}
