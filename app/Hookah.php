<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hookah extends Model implements \App\Contracts\UserCountableInterface
{
    /**
     * Maximum number of users allowed for each pipe
     */
    public const USERS_PER_PIPE = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'bar_id',
        'name',
        'pipes_count',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'bar_id' => 'integer',
        'name' => 'string',
        'pipes_count' => 'integer',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bar()
    {
        return $this->belongsTo('App\Bar');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reservations()
    {
        return $this->hasMany('App\Reservation');
    }

    /**
     * @inheritDoc
     */
    public function getUserCountAttribute(): int
    {
        return $this->pipes_count * static::USERS_PER_PIPE;
    }
}
