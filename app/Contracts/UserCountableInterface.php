<?php

namespace App\Contracts;

interface UserCountableInterface
{
    /**
     * @return int
     */
    public function getUserCountAttribute(): int;
}
