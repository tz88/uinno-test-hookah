<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Hookah extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'pipes_count' => $this->pipes_count,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            //'reservations' => Reservation::collection($this->reservations),
        ];
    }
}
