<?php

namespace App\Http\Requests;

class AvailableHookah extends \App\Http\Requests\base\BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_count' => ['required', 'integer', 'between:1,20'],
            'from' => ['required', 'date', 'after:now'],
            'to' => ['required', 'date', 'after:now'],
        ];
    }
}
