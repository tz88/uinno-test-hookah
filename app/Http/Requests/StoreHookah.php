<?php

namespace App\Http\Requests;

class StoreHookah extends \App\Http\Requests\base\BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        /**
         * I haven't found rule that describes unique rule for combination of 2 fields (name + bar_id)
         * the only working solution is to pass real value from request to rules
         */
        $barId = $this->route()->parameter('bar');
        return [
            'name' => ['required', 'string', 'unique:hookahs,name,NULL,id,bar_id,' . $barId, 'max:255'],
            'pipes_count' => ['required', 'integer', 'between:1,10'],
        ];
    }
}
