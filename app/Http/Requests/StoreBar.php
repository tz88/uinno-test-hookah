<?php

namespace App\Http\Requests;

class StoreBar extends \App\Http\Requests\base\BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'unique:bars', 'max:255'],
        ];
    }
}
