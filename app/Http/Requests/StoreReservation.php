<?php

namespace App\Http\Requests;

use App\Rules\RuleFactory;

class StoreReservation extends \App\Http\Requests\base\BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $hookahId = $this->route()->parameter('hookah');
        return [
            'name' => ['required', 'string', 'max:255'],
            'user_count' => [
                'required',
                'integer',
                'between:1,20',
                RuleFactory::create(
                    RuleFactory::RULE_USER_COUNT_PIPES_SATISFY,
                    ['hookahId' => $hookahId]
                )
            ],
            'start_date' => [
                'bail',
                'required',
                'date',
                'after:now',
                RuleFactory::create(
                    RuleFactory::RULE_DATE_HOOKAH_AVAILABILITY,
                    ['hookahId' => $hookahId]
                )
            ],
        ];
    }
}
