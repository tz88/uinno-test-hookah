<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReservation;
use App\Repositories\Contracts\BarRepositoryInterface;
use App\Repositories\Contracts\ReservationRepositoryInterface;

class ReservationController extends Controller
{
    /**
     * The reservations repository instance.
     *
     * @var \App\Repositories\Contracts\ReservationRepositoryInterface
     */
    protected $reservations;

    /**
     * Create a new controller instance.
     *
     * @param \App\Repositories\Contracts\ReservationRepositoryInterface $reservations
     * @return void
     */
    public function __construct(ReservationRepositoryInterface $reservations)
    {
        $this->reservations = $reservations;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $barId
     * @return \Illuminate\Http\Response
     */
    public function index(int $barId, BarRepositoryInterface $barRepository)
    {
        $bar = $barRepository->get($barId);
        return $this->reservations->all($bar);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreReservation $request
     * @param int $hookahId
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReservation $request, int $hookahId)
    {
        return $this->reservations->create($hookahId, $request->validated());
    }
}
