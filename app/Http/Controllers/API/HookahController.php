<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\AvailableHookah;
use App\Http\Requests\StoreHookah;
use App\Repositories\Contracts\HookahRepositoryInterface;
use App\Services\Contracts\ComboGenerator;
use App\Services\Contracts\HookahFindAvailableInterface;

class HookahController extends Controller
{
    /**
     * The hookahs repository instance.
     *
     * @var \App\Repositories\Contracts\HookahRepositoryInterface
     */
    protected $hookahs;

    /**
     * Create a new controller instance.
     *
     * @param \App\Repositories\Contracts\HookahRepositoryInterface $hookahs
     * @return void
     */
    public function __construct(HookahRepositoryInterface $hookahs)
    {
        $this->hookahs = $hookahs;
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $barId
     * @return \Illuminate\Http\Response
     */
    public function index(int $barId)
    {
        return $this->hookahs->all($barId);
    }

    /**
     * Display a list of available hookahs for provided number of users and date period.
     *
     * @param \App\Http\Requests\AvailableHookah $request
     * @param int $barId
     * @param \App\Services\Contracts\HookahFindAvailableInterface $hookahFinder
     * @param \App\Services\Contracts\ComboGenerator $generator
     * @return \Illuminate\Http\Response
     */
    public function available(
        AvailableHookah $request,
        int $barId,
        HookahFindAvailableInterface $hookahFinder,
        ComboGenerator $generator
    ) {
        $data = $request->validated();
        // getting hookahs that satisfy request params
        $hookahs = $hookahFinder->find($barId, $data);
        // generating combinations of hookahs that satisfy needed user_count
        return $generator->generate($hookahs, $data['user_count']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreHookah $request
     * @param int $barId
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHookah $request, int $barId)
    {
        return $this->hookahs->create($barId, $request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $hookahId
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $hookahId)
    {
        $this->hookahs->delete($hookahId);
    }
}
