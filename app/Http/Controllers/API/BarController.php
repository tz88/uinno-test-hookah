<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBar;
use App\Repositories\Contracts\BarRepositoryInterface;

class BarController extends Controller
{
    /**
     * The bars repository instance.
     *
     * @var \App\Repositories\Contracts\BarRepositoryInterface
     */
    protected $bars;

    /**
     * Create a new controller instance.
     *
     * @param \App\Repositories\Contracts\BarRepositoryInterface $bars
     * @return void
     */
    public function __construct(BarRepositoryInterface $bars)
    {
        $this->bars = $bars;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->bars->all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\StoreBar $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBar $request)
    {
        return $this->bars->create($request->validated());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $barId
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $barId)
    {
        $this->bars->delete($barId);
    }
}
