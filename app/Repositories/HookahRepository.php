<?php

namespace App\Repositories;

use App\Hookah;
use App\Http\Resources\HookahCollection;

class HookahRepository implements \App\Repositories\Contracts\HookahRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function get(int $id): Hookah
    {
        return Hookah::findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function all(int $barId): HookahCollection
    {
        return new HookahCollection(Hookah::where('bar_id', $barId)->paginate());
    }

    /**
     * @inheritDoc
     */
    public function create(int $barId, array $data): Hookah
    {
        $hookah = new Hookah();
        $hookah->fill($data);
        $hookah->bar_id = $barId;
        $hookah->save();
        return $hookah;
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id)
    {
        $hookah = $this->get($id);
        $hookah->delete();
    }

    /**
     * @return mixed
     */
    public function select()
    {
        return Hookah::select();
    }

    /**
     * Scope by bar_id
     * @param $query
     * @param $barId
     * @return mixed
     */
    public function byBar($query, $barId)
    {
        return $query->where('bar_id', $barId);
    }
}
