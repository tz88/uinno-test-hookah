<?php

namespace App\Repositories;

use App\Reservation;
use App\Http\Resources\ReservationCollection;

class ReservationRepository implements \App\Repositories\Contracts\ReservationRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function get(int $id): Reservation
    {
        return Reservation::findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function all($bar)
    {
        return new ReservationCollection($bar->reservations()->paginate());
    }

    /**
     * @inheritDoc
     */
    public function create(int $hookahId, array $data): Reservation
    {
        $reservation = new Reservation();
        $reservation->fill($data);
        $reservation->hookah_id = $hookahId;
        /**
         * We are allowed to book hookahs only for 30 mins, not less and not more
         * that's why end_date is calculated automatically and can't be passed via request
         */
        $endDate = new \DateTime($data['start_date']);
        $reservation->end_date = $endDate->modify(Reservation::END_DATE_MODIFIER);
        $reservation->save();
        return $reservation;
    }

    /**
     * @return mixed
     */
    public function select()
    {
        return Reservation::select();
    }

    /**
     * Scope by date - search all reservations that cross provided time diapason
     *
     * @param $query
     * @param string $startDate
     * @param string|null $endDate
     * @return mixed
     */
    public function byReservationDate(
        $query,
        string $startDate,
        ?string $endDate = null
    ) {
        $startDate = new \DateTime($startDate);
        if (null === $endDate) {
            $endDate =  clone $startDate;
            $endDate->modify(Reservation::END_DATE_MODIFIER);
        } else {
            $endDate = new \DateTime($endDate);
        }
        return $query->where(function($query) use ($startDate, $endDate) {
            $query->where([
                ['start_date', '>=', $startDate],
                ['start_date', '<', $endDate]
            ])->orWhere([
                ['end_date', '>', $startDate],
                ['end_date', '<=', $endDate]
            ]);
        });
    }

    /**
     * Check if there already are reservations for given dates
     * For this case we need to check:
     * 1. reservations where start_date is in active diapason
     * 2. reservations where end_date is in active diapason
     *
     * @param int $hookahId
     * @param string $startDate
     * @param string|null $endDate
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getByDateDiapason(
        int $hookahId,
        string $startDate,
        ?string $endDate = null
    ): \Illuminate\Database\Eloquent\Builder {
        $query = Reservation::where('hookah_id', $hookahId);
        return $this->byReservationDate($query, $startDate, $endDate);
    }
}
