<?php

namespace App\Repositories\Contracts;

use App\Bar;

interface BarRepositoryInterface
{
    /**
     * Get hookah
     * @param int $id
     * @return \App\Bar
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function get(int $id): Bar;

    /**
     * Get list of bars
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function all();

    /**
     * Create new bar instance
     * @param array $data
     * @return \App\Bar
     * @throws \Illuminate\Database\QueryException
     */
    public function create(array $data): Bar;

    /**
     * Delete a bar
     * @param int $id
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function delete(int $id);
}
