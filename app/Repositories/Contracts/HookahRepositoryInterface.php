<?php

namespace App\Repositories\Contracts;

use App\Hookah;
use App\Http\Resources\HookahCollection;

interface HookahRepositoryInterface
{
    /**
     * Get hookah
     * @param int $id
     * @return \App\Hookah
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function get(int $id): Hookah;

    /**
     * Get list of hookahs for specific bar
     * @param int $barId
     * @return \App\Http\Resources\HookahCollection
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function all(int $barId): HookahCollection;

    /**
     * Create new hookah instance for specific bar
     * @param int $barId
     * @param array $data
     * @return \App\Hookah
     * @throws \Illuminate\Database\QueryException
     */
    public function create(int $barId, array $data): Hookah;

    /**
     * Delete a hookah
     * @param int $id
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function delete(int $id);
}
