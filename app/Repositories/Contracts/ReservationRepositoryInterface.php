<?php

namespace App\Repositories\Contracts;

use App\Reservation;

interface ReservationRepositoryInterface
{
    /**
     * Get hookah
     * @param int $id
     * @return \App\Reservation
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function get(int $id): Reservation;

    /**
     * Get list of reservations for specific hookah
     * @param \App\Bar $bar
     * @return mixed
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function all($bar);

    /**
     * Create new reservation instance for specific hookah
     * @param int $hookahId
     * @param array $data
     * @return \App\Reservation
     * @throws \Illuminate\Database\QueryException
     */
    public function create(int $hookahId, array $data): Reservation;
}
