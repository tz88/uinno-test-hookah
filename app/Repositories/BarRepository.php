<?php

namespace App\Repositories;

use App\Bar;
use App\Http\Resources\BarCollection;

class BarRepository implements \App\Repositories\Contracts\BarRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function get(int $id): Bar
    {
        return Bar::findOrFail($id);
    }

    /**
     * @inheritDoc
     */
    public function all()
    {
        return new BarCollection(Bar::paginate());
    }

    /**
     * @inheritDoc
     */
    public function create(array $data): Bar
    {
        return Bar::create($data);
    }

    /**
     * @inheritDoc
     */
    public function delete(int $id)
    {
        $bar = Bar::findOrFail($id);
        $bar->delete();
    }
}
