<?php

namespace Tests\Unit;

use Tests\TestCase;

class HookahTest extends TestCase
{
    /**
     * ComboGenerator service test (used in Hookah::available action).
     *
     * @return void
     */
    public function testComboGenerator()
    {
        // mocking simple collection of hookahs
        $hookahs = [
            factory(\App\Hookah::class)->make(['pipes_count' => 3, 'id' => 1]),
            factory(\App\Hookah::class)->make(['pipes_count' => 1, 'id' => 2]),
            factory(\App\Hookah::class)->make(['pipes_count' => 2, 'id' => 3]),
            factory(\App\Hookah::class)->make(['pipes_count' => 4, 'id' => 4])
        ];

        // generating available combinations to satisfy 5 users
        $comboGenerator = $this->app->make(\App\Services\Contracts\ComboGenerator::class);
        $result = $comboGenerator->generate($hookahs, 5);

        // check results
        $this->assertIsArray($result);
        // check that we got 3 combinations
        $this->assertEquals(3, count($result));
        // check that first hookah is one in combo and comes first
        $this->assertEquals(1, $result[0][0]->id);
        $this->assertEquals(1, count($result[0]));
        // check last hookahs combination has 2 hookahs (satisfy userCount together)
        $this->assertEquals(2, count($result[2]));
    }
}
