<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;

class HookahTest extends TestCase
{
    // rollback changes after each test
    use DatabaseTransactions;

    /**
     * Hookah store test
     *
     * @return void
     */
    public function testStore()
    {
        // prepare parent relations
        $bar = factory(\App\Bar::class)->create();

        // send request
        $name = 'My test Gookah for Bar #' . $bar->id;
        $pipesCount = rand(1, 4);
        $response = $this->postJson(
            "/api/bars/{$bar->id}/hookahs",
            ['name' => $name, 'pipes_count' => $pipesCount]
        );

        // data of created hookah can be obtained from $response->getContent()

        // check result
        $response->assertStatus(201)
            ->assertJson([
                'name' => $name,
                'pipes_count' => $pipesCount,
            ]);
    }
}
