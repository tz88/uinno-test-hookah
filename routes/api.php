<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// bars
Route::apiResource('bars', 'API\BarController')->only([
    'index', 'store', 'destroy'
]);

// hookahs
Route::apiResource('bars.hookahs', 'API\HookahController')->only([
    'index', 'store', 'destroy'
])->shallow();
Route::get('bars/{bar}/hookahs/available', 'API\HookahController@available')
    ->name('bars.hookahs.available');

// reservations
Route::apiResource('hookahs.reservations', 'API\ReservationController')->only([
    'store'
])->shallow();
Route::get('bars/{bar}/reservations', 'API\ReservationController@index')
    ->name('bars.hookahs.reservations.index');

