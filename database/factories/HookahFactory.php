<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Hookah;
use Faker\Generator as Faker;

$factory->define(Hookah::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'pipes_count' => $faker->numberBetween(1, 4),
    ];
});
