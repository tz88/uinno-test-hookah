<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Reservation;
use Faker\Generator as Faker;

$factory->define(Reservation::class, function (Faker $faker) {
    // ideally this dates should be unique and time should not match but for current test that should be ok
    $startDate = $faker->dateTimeThisMonth();
    $endDate = clone $startDate;
    $endDate->modify("+30 minutes");
    return [
        'name' => $faker->name,
        'user_count' => $faker->numberBetween(2, 8),
        'start_date' => $startDate,
        'end_date' => $endDate,
    ];
});
