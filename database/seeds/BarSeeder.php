<?php

use Illuminate\Database\Seeder;

class BarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // building 3 bars
        factory(App\Bar::class, 3)->create()->each(function ($bar) {
            // with 7 hookahs in each
            factory(App\Hookah::class, 7)->create(['bar_id' => $bar->id])->each(function ($hookah) {
                // with 3 reservation per each hookah
                $hookah->reservations()->createMany(
                    factory(App\Reservation::class, 3)->make()->toArray()
                );
            });
        });
    }
}
