## DB

- bars: id, name
  
- hookahs: id, bar_id, name, pipes_count
  
- reservations: id, hookah_id, name, user_count, start_date, end_date

## REST

Bars:

**GET /api/bars**: list of bars

**POST /api/bars**: (params: name={\s}) create bar

**DELETE /api/bars/{id}**: delete specific bar

Hookahs:

**GET /api/bars/{barId}/hookahs**: list of all hookahs for specific bar

**GET /api/bars/{barId}/hookahs/available**: (params: user_count={\d}; from={TIMESTAMP}; to={TIMESTAMP}): list of available hookah for chosen period (from/to in TIMESTAMP format) and user_count. Notes: should return different possible combinations of hookah satisfying user_count value, e.g: [[hookahA], [hookahB, hookahC], [hookahB, hookahD], [hookahC, hookahD], [hookahD, hookahE, hookahF], ...]

**POST /api/bars/{barId}/hookahs**: create hookah for specific bar

**DELETE /api/hookahs/{id}**: delete specific hookah

Reservations:

**GET /api/bars/{barId}/reservations**: list of reservations for specific bar (should we exclude past reservations?)

**POST /api/hookahs/{hookahId}/reservations** (params: name={\s}; user_count={\d}, start_date={TIMESTAMP}): reserve hookah

## Deploy

* `git clone git@gitlab.com:tz88/uinno-test-hookah.git`
* `cd uinno-test-hookah`
* `composer install`
* `touch database/database.sqlite`
* `touch database/test.sqlite`
* `cp .env.example .env`
* `php artisan serve` (in separate terminal)
* `php artisan migrate:fresh --seed`
* `php artisan migrate:fresh --env=testing`
* `php artisan route:list`
* `php artisan test`

## General flow

1. POST /api/bars ->  create new Bar
2. POST /api/bars/{barId}/hookahs -> create new Hookah in the Bar
3. GET /api/bars/{barId}/hookahs/available -> check which Hookahs are available for provided time diapason and number of users (should return not a simple hookahs collection but list of hookah combinations that satisfy number of users)
4. POST /api/hookahs/{hookahId}/reservations -> reserve specific Hookahs (1 Hookah per request)
5. GET /api/bars/{barId}/reservations -> check all reservations for chosen Bar (to check list of Users and what Hookahs they booked)
